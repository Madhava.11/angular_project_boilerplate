import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent {
  username: string = '';
  password: string = '';
  error: string = '';

  constructor(private authService: AuthenticationService, private router: Router) {}

  login(): void {
    this.authService.login(this.username, this.password).subscribe((success: boolean) => {
      if (success) {
        this.router.navigate(['/home']);
      } else {
        this.error = 'Invalid username or password';
      }
    });
  }

}
