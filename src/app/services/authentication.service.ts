import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor() {}

  login(username: string, password: string): Observable<boolean> {
    // Dummy login logic (replace with actual authentication logic)
    if (username === 'user' && password === 'password') {
      localStorage.setItem('currentUser', JSON.stringify({ username: username }));
      return of(true);
    } else {
      return of(false);
    }
  }

  logout(): void {
    localStorage.removeItem('currentUser');
  }

  signup(username: string, password: string): Observable<boolean> {
    // Dummy signup logic (replace with actual signup logic)
    // For demonstration, assume signup is successful
    return of(true);
  }
}
