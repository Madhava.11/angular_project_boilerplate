import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path:'home',
    loadChildren: ()=>import('./modules/dashboard/dashboard.module').then(m=>m.DashboardModule)
  },
  {
    path:'signin',
    loadChildren: ()=>import('./modules/signin/signin.module').then(m=>m.SigninModule)
  },
  {
    path:'login',
    loadChildren: ()=>import('./modules/login/login.module').then(m=>m.LoginModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
